import React from 'react';
import ReactDom from 'react-dom';

export default () => (<div>Uds-sections</div>);

export const mount = (Сomponent) => {
    ReactDom.render(
        <Сomponent/>,
        document.getElementById('app')
    );
}


export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'))
}



